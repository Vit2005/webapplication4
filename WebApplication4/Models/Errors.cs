﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication4.Models
{
    public class Errors
    {
        private static Errors _instatse;

        public static Errors Instatse
        {
            get { return _instatse; }
        }

        private Dictionary<int, object> errors;

        public Errors(string json) // {"1":{"name1":"value1","name2":"value2"},"OK":{"name1":"value1","name2":"value2"}, ... }
        {
            errors = JsonConvert.DeserializeObject<Dictionary<int, object>>(json);
            _instatse = this;
        }

        public Dictionary<string, string> GetError(int key)
        {
            if (errors.ContainsKey(key))
                return JsonConvert.DeserializeObject<Dictionary<string, string>>(errors[key].ToString());
            else
                return null;
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication4.Models;
using System.Web.Script.Serialization;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Facebook()
        {
            return View();
        }

        public ActionResult ExternalLoginResult(string code, string state)
        {
            string req = "https://api.odnoklassniki.ru/oauth/token.do?code=" + code +
                "&client_id=1126962432&client_secret=9DE653024015EEAC28F48FE7" +
                "&redirect_uri=http://localhost:18080/Home/ExternalLoginResult" +
                "&grant_type=authorization_code" +
                "&scope=VALUABLE_ACCESS";

            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(OdnoklassnikiModel.Instatse.SendData(req, url: ""));
            OdnoklassnikiModel.AccessToken = values["access_token"];
            string sigSecret = OdnoklassnikiModel.Instatse.md5string(OdnoklassnikiModel.AccessToken);

            // ========== default query ===========
            //string data = "application_key=CBAGBNCEEBABABABA&fields=uid,first_name,last_name&uids=254876682293";
            //string req2 = "api/users/getInfo?" + data +
            //    "&sig=" + OdnoklassnikiModel.Instatse.md5string(data.Replace("&", "") + sigSecret, "") +
            //    "&access_token=" + OdnoklassnikiModel.AccessToken;

            // ========== get uid query ===========
            string data = "application_key=CBAGBNCEEBABABABA&fields=uid"; //uid,first_name,last_name
            string req2 = "api/users/getCurrentUser?" + data +
                "&sig=" + OdnoklassnikiModel.Instatse.md5string(data.Replace("&", "") + sigSecret, "") +
                "&access_token=" + OdnoklassnikiModel.AccessToken;

            values = JsonConvert.DeserializeObject<Dictionary<string, string>>(OdnoklassnikiModel.Instatse.SendData(req2));
            OdnoklassnikiModel.uid = values["uid"];
            // ========== payment query ===========
            Random r = new Random();
            string call_id = r.Next(1000000000).ToString();
            string transaction_id = r.Next(1000000000).ToString();
            string transaction_time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            //string data3 = "amount=1" +
            //    "&application_key=" + OdnoklassnikiModel.ApplicationKey +
            //    "&call_id=" + call_id +
            //    //"&method=callbacks.payment" +
            //    "&product_code=IDDQD" +
            //    //"&session_key=null" +
            //    "&transaction_id=" + transaction_id +
            //    "&transaction_time=" + transaction_time +
            //    "&uid=" + OdnoklassnikiModel.uid;
            //string req3 = "api/callbacks/payment?" + data3 +
            //    "&sig=" + OdnoklassnikiModel.Instatse.md5string(data3.Replace("&", "") + sigSecret, "") +
            //    "&access_token=" + OdnoklassnikiModel.AccessToken;

            string data3 = "application_key=" + OdnoklassnikiModel.ApplicationKey +
                "&method=users.getAdditionalInfo&uids=" + OdnoklassnikiModel.uid;
            string req3 = "fb.do?" + data3 +
                "&sig=" + OdnoklassnikiModel.Instatse.md5string(data3.Replace("&", "") + sigSecret, "") +
                "&access_token=" + OdnoklassnikiModel.AccessToken;

            string resp = OdnoklassnikiModel.Instatse.SendData(req3, queryType: "GET");
            resp = string.Format("{0}\n{1}", req3, resp);
            return new ContentResult
            {
                Content = resp,
                ContentType = "text/plain"
            };
        }

        public ActionResult VkLoginResult(string code)
        {
            //string req = "https://oauth.vk.com/access_token?client_id=4830147&client_secret=B3sNnxQTJmHud1dpbqB1&v=5.29&grant_type=client_credentials";
            string req = "https://oauth.vk.com/access_token?" +
                "client_id=" + OdnoklassnikiModel.ApplicationKey +
                "&client_secret=" + OdnoklassnikiModel.PrivateKey +
                "&redirect_uri=" + "http://localhost:18080/Home/VkLoginResult" +
                "&code=" + code;

            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(OdnoklassnikiModel.Instatse.SendData(req, url: ""));
            OdnoklassnikiModel.AccessToken = values["access_token"];

            // =========== default query query =============
            //string req2 = "https://api.vk.com/method/users.get?user_id=66748&v=5.29";//&access_token=" + OdnoklassnikiModel.AccessToken;
            //string resp = OdnoklassnikiModel.Instatse.SendData(req2, url: "");

            //string req2 = "https://vk.com/dev/secure.getAppBalance";

            //return new ContentResult
            //{
            //    Content = resp,
            //    ContentType = "text/plain"
            //};
            return View();
        }

        public ActionResult VkServerLoginResult()
        {
            var oSR = new StreamReader(Request.InputStream);
            string sContent = oSR.ReadToEnd();
            System.Web.HttpContext.Current.Application["log"] += " Vkontakte Request=" + sContent;
            return new ContentResult
            {
                Content = SocialPayment.Instatse.VKResponse(sContent)
            };
        }

        public ActionResult OkServerLoginResult()
        {
            string result = SocialPayment.Instatse.OKResponse(Request.QueryString);
            if (result != "success")
                Response.AppendHeader("Invocation-error", result);
            return new ContentResult
            {
                ContentType = "application/xml",
                Content = "response" //Odnoklassniki don't need a response
            };
        }

        public ActionResult FbServerLoginResult()
        {
            var oSR = new StreamReader(Request.InputStream);
            string sContent = oSR.ReadToEnd();
            System.Web.HttpContext.Current.Application["log"] += " Facebook Request=" + sContent;
            if ((Request != null) && (Request.QueryString.AllKeys.Contains("hub.challenge"))) //for test query
                return new ContentResult
                {
                    Content = Request.QueryString["hub.challenge"]
                }; 
            string result = SocialPayment.Instatse.FBResponse(sContent);
            if (result != "success")
                Response.AppendHeader("Invocation-error", result);

            return new ContentResult
            {
                Content = "response" //Facebook don't need a response
            };
        }

        public ActionResult MrServerLoginResult()
        {
            return new ContentResult
            {
                Content = SocialPayment.Instatse.MRResponse(Request.QueryString)
            };
        }

        public ActionResult GetLog()
        {
            return new ContentResult
            {
                Content = System.Web.HttpContext.Current.Application["log"].ToString()
            };
            
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication4.Models
{
    public class Prices
    {
        private static Prices _instatse;

        public static Prices Instatse
        {
            get { return _instatse; }
        }

        private Dictionary<string, object> prices;

        public Prices(string json) // {"VK":{"name1":"value1","name2":"value2"},"OK":{"name1":"value1","name2":"value2"}, ... }
        {
            prices = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            _instatse = this;
        }

        public Dictionary<string, string> GetPrices(string key)
        {
            if (prices.ContainsKey(key))
                return JsonConvert.DeserializeObject<Dictionary<string, string>>(prices[key].ToString());
            else
                return null;
        }


    }
}
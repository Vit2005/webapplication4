﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using System.Xml.Linq;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebApplication4.Models
{
    public class SocialPayment
    {
        private static SocialPayment _instatse;

        //private string VkApplicationKey = "4830147";
        private string VkPrivateKey = "loTCO8vQPfsyEuBA4ySn";
        //private string OkApplicationKey = "CBAGBNCEEBABABABA";
        private string OkPrivateKey = "9DE653024015EEAC28F48FE7";
        private string FbApplicationKey = "1608873762665013";
        private string FbPrivateKey = "3c26f41277c9606cb957db3ccd669e48";
        private string MrSecretKey = "01d4245ac5ca7c6fdd9ca30afdda3d32";

        public string FacebookApplicationAccessToken = "";

        public static SocialPayment Instatse
        {
            get { return _instatse; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public SocialPayment()
        {
            //TODO: read data from json file
            Prices p = new Prices("{\"VK\": {\"1life\":\"1\",\"2life\":\"2\"} }");
            Errors e = new Errors("{1:{\"error_msg\":\"unknown request\", \"critical\": \"false\"}," +
                                  "2:{\"error_msg\":\"connection to database failed\", \"critical\": \"false\"}," +
                                   "10:{\"error_msg\":\"item not wxist\", \"critical\": \"true\"}}");
            //TODO: initialize public and private keys here

            System.Web.HttpContext.Current.Application.Lock();
            System.Web.HttpContext.Current.Application["log"] = "";
            System.Web.HttpContext.Current.Application.UnLock();
            _instatse = this;
        }

        /// <summary>
        /// Method, called from controller, which requires query string
        /// </summary>
        /// <param name="query">Query, sended to method, whitch will be processed</param>
        /// <returns>Response string, which must be sended by controller as ContentResult</returns>
        public string VKResponse(string query)
        {
            Dictionary<string, object> mainObject = new Dictionary<string, object>();
            Dictionary<string, object> dataObject = new Dictionary<string, object>();
            try
            {
                Dictionary<string, string> requestParams = VkParseQuery(query);
                if (!VKCheckMd5(requestParams))
                {
                    mainObject = VKerror(1); //unknown request
                    return (new JavaScriptSerializer()).Serialize(mainObject);
                }
                switch (requestParams["notification_type"])
                {
                    case "get_item_test":
                    case "get_item":
                        //Income parameters:
                        //notification_type, app_id, user_id, receiver_id, order_id, lang, item, sig
                        //Outcome parameters: 
                        //title, photo_url, price, item_id, expiration
                        //Details about parameters: https://vk.com/dev/payments_getitem
                        Dictionary<string, string> prices = Prices.Instatse.GetPrices("VK");
                        if (prices.ContainsKey(requestParams["item"]))
                        {
                            dataObject.Add("title", VKquestion(requestParams["lang"]) + requestParams["item"] + "?");
                            dataObject.Add("price", prices[requestParams["item"]]);
                            mainObject.Add("response", dataObject);
                            break;
                        }
                        else
                        {
                            VKerror(10);//item not wxist
                        }
                        break;
                    case "order_status_change":
                    case "order_status_change_test":
                        //Income parameters:
                        //notification_type, app_id, user_id, receiver_id, order_id, date, status, sig, item, item_id, item_title, item_photo_url, item_price
                        //Outcome parameters: 
                        //order_id, app_order_id
                        //Details about parameters: https://vk.com/dev/payments_status
                        if (!SomeDatabaseMethod(requestParams["user_id"], requestParams["item"], "", "Vkontakte"))
                            VKerror(2); // connection to database failed
                        

                        dataObject.Add("order_id", requestParams["order_id"]);
                        mainObject.Add("response", dataObject);
                        break;
                    default:
                        VKerror(1); // unknown request
                        break;
                }
            }
            catch (Exception e)
            {
                dataObject.Add("error_code", "500");
                dataObject.Add("error_msg", e.ToString());
                dataObject.Add("critical", "false");
                mainObject.Add("error", dataObject);
                System.Web.HttpContext.Current.Application["log"] += " Vkontakte exception=" + e.ToString();
            }

            return (new JavaScriptSerializer()).Serialize(mainObject);
        }

        /// <summary>
        /// Method, called from controller, which requires query Request.QueryString parameter
        /// </summary>
        /// <param name="query">Request.QueryString from controller</param>
        /// <returns>String "success" if query successfuly, or string with code of error</returns>
        public string OKResponse(NameValueCollection query)
        {
            if (!CheckMd5(query, OkPrivateKey))
            {
                return "104"; // incorrect signature http://apiok.ru/wiki/pages/viewpage.action?pageId=46137373#APIДокументация(Русский)-callbacks.payment
            }
            //Income parameters:
            //uid, transaction_time, transaction_id, product_code, product_option, amount, currency, payment_system, extra_attributes
            //http://apiok.ru/wiki/display/api/callbacks.payment+ru

            

            if (!SomeDatabaseMethod(query["uid"], query["product_code"], query["amount"], "Odnoklassniki")) // cannot connect to database or database sends an error
            {
                return "2";
            }

            return "success";
        }

        /// <summary>
        /// Method, called from controller, which requires query Request.QueryString parameter
        /// </summary>
        /// <param name="query">Query, sended to method, whitch will be processed</param>
        /// <returns>String "success" if query successfuly, or string with code of error</returns>
        /// <example>Query example: {"object":"payments","entry":[{"id":"654962377967195","time":1427192852,"changed_fields":["actions"]}]}</example>
        public string FBResponse(string query)
        {
            Dictionary<string, object> q = JsonConvert.DeserializeObject<Dictionary<string, object>>(query); // get dictionary from string
            JArray data = JsonConvert.DeserializeObject<JArray>(q["entry"].ToString()); // get array of "entry"
            Dictionary<string, object> entry = JsonConvert.DeserializeObject<Dictionary<string, object>>(data[0].ToString()); // get dictionary of first entry
            
            string responseString = FbSendQuery(string.Format("https://graph.facebook.com/v2.2/{2}?access_token={0}|{1}&format=json&method=get&pretty=0&suppress_http_code=1", FbApplicationKey, FbPrivateKey, entry["id"].ToString())); // sending query to Facebook server to get information about item and user
            // example response string: {"id":"600059266791040","user":{"id":"1429552830676223","name":"Lin Airin"},"application":{"name":"TestPayment","id":"1608873762665013"},"actions":[{"type":"charge","status":"completed","currency":"USD","amount":"5.98","time_created":"2015-03-23T15:27:18+0000","time_updated":"2015-03-23T15:27:32+0000"}],"refundable_amount":{"currency":"USD","amount":"5.98"},"items":[{"type":"IN_APP_PURCHASE","product":"https:\/\/dl.dropboxusercontent.com\/u\/6474542\/work_\/temp\/price600.html","quantity":2}],"country":"UA","created_time":"2015-03-23T15:27:18+0000","test":1,"payout_foreign_exchange_rate":1,"tax":"not_taxed","tax_country":"UA"}
            System.Web.HttpContext.Current.Application["log"] += " Response=" + responseString; // save response to log
            Dictionary<string, object> responseObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseString); // parse response string into array
            JArray responseItems = JsonConvert.DeserializeObject<JArray>(responseObject["items"].ToString()); // get array of "items" from dictionary
            Dictionary<string, string> responseItem = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseItems[0].ToString()); // get first item of "items" dictionary
            Dictionary<string, string> responseUser = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseObject["user"].ToString()); // get user from response string

            if (!SomeDatabaseMethod(responseUser["id"], responseItem["product"], responseItem["quantity"], "Facebook"))
                return "500";
            
            return "success";
        }

        public string MRResponse(NameValueCollection query)
        {
            Dictionary<string, object> dataObject = new Dictionary<string, object>();
            if (!CheckMd5(query, MrSecretKey))
            {
                dataObject.Add("status", "2");
                dataObject.Add("error_code", "702"); //http://api.mail.ru/docs/guides/billing/
                return (new JavaScriptSerializer()).Serialize(dataObject);
            }
                
            if (SomeDatabaseMethod(query["uid"], query["service_id"], "", "MailRu"))
            {
                dataObject.Add("status", "1");
                return (new JavaScriptSerializer()).Serialize(dataObject);
            }

            dataObject.Add("status", "0");
            dataObject.Add("error_code", "700");
            return (new JavaScriptSerializer()).Serialize(dataObject);
        }

        #region VK

        /// <summary>
        /// Converts query string to dictionary with keys and values
        /// </summary>
        /// <param name="query">string which will be processed</param>
        /// <returns>Dictionary with keys and values</returns>
        private Dictionary<string, string> VkParseQuery(string query)
        {
            Dictionary<string, string> requestParams = new Dictionary<string, string>();
            query = HttpContext.Current.Server.UrlDecode(query);
            string[] kvAll = query.Split(new char[] { '&' });
            foreach (string kv in kvAll)
            {
                string[] kvArray = kv.Split(new char[] { '=' });
                requestParams.Add(kvArray[0], kvArray[1]);
            }
            return requestParams;
        }

        /// <summary>
        /// Checks correct signature
        /// </summary>
        /// <param name="kv">Dictionary with keys and values</param>
        /// <returns>Boolean parameter which shows is signature is correct</returns>
        private bool VKCheckMd5(Dictionary<string, string> kv)
        {
            List<string> keys = kv.Keys.ToList<string>();
            keys.Sort();
            keys.Remove("sig");
            if (keys.Contains("item_title"))
            {
                kv["item_title"] = HttpContext.Current.Server.UrlDecode(kv["item_title"]); // url converts utf-8 to unicode, so we need decode unicode
            }
            string result = string.Empty;
            foreach (string k in keys)
            {
                result += k + "=" + kv[k];
            }
            result += this.VkPrivateKey;
            return kv["sig"] == GetMD5Hash(result);
        }

        /// <summary>
        /// Method returns dictionary for converting to Json as error
        /// </summary>
        /// <param name="errorId">Identifier of error</param>
        /// <returns>Dictionary with error description</returns>
        private Dictionary<string, object> VKerror(int errorId)
        {
            Dictionary<string, object> mainObject = new Dictionary<string, object>();
            Dictionary<string, object> dataObject = new Dictionary<string, object>();
            Dictionary<string, string> error = Errors.Instatse.GetError(errorId);
            dataObject.Add("error_code", errorId);
            dataObject.Add("error_msg", error["error_msg"]);
            dataObject.Add("critical", error["critical"]);
            mainObject.Add("error", dataObject);
            return mainObject;
        }

        /// <summary>
        /// Method returns title of question which depends on language
        /// </summary>
        /// <param name="lang">Language (valiable languages written here: https://vk.com/dev/payments_getitem )</param>
        /// <returns>String with title</returns>
        private string VKquestion(string lang)
        {
            switch (lang)
            {
                case "ru_RU": return "Вы действительно хотите купить ";
                case "uk_UA": return "Ви дійсно бажаєте придбати ";
                case "be_BY": return "Вы сапраўды хочаце купіць ";
                case "en_US": return "Do you really want to buy ";
                default: return "";
            }
        }

        #endregion

        #region FB

        /// <summary>
        /// Sending query method
        /// </summary>
        /// <param name="url">Query url, which will be sended</param>
        /// <returns>Response string</returns>
        private string FbSendQuery(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/x-www-form-urlencoded";

            var response = (HttpWebResponse)request.GetResponse();

            if (response != null)
            {
                var strreader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                var responseToString = strreader.ReadToEnd();
                return responseToString;
            }
            return "";
        }

        #endregion

        private bool CheckMd5(NameValueCollection kv, string SecretKey)
        {
            List<string> keys = new List<string>();
            foreach (string key in kv.Keys)
            {
                keys.Add(key);
            }
            keys.Sort();
            keys.Remove("sig");
            string result = string.Empty;
            foreach (string k in keys)
            {
                result += k + "=" + kv[k];
            }
            result += SecretKey;
            return kv["sig"] == GetMD5Hash(result);
        }

        /// <summary>
        /// Method gets md5 hash from string
        /// </summary>
        /// <param name="input">String that take md5</param>
        /// <returns>String with md5 hash</returns>
        private string GetMD5Hash(string input)
        {
            var x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            var bs = Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            var s = new StringBuilder();
            foreach (var b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }

        private bool SomeDatabaseMethod(string uid, string item, string count, string socialnetwork)
        {
            //ENTER YOUR CONNECTION TO DATABASE CODE HERE
            System.Web.HttpContext.Current.Application["log"] += string.Format("SomeDatabaseMethod({0},{1},{2}) ---{3}---", uid, item, count, socialnetwork);
            return true;
        }

    }
}